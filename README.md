Lektor-basic-theme
==================

A Minimalist, responsive theme for lektor.

**Note:** This theme doesn't change layout file(html) instead use few lines of js and css along with jQuery and bootstrap to do the same.

![Screenshot](screenshot.png)
## Pre-requisite

Lektor must be [installed](https://www.getlektor.com/docs/installation/) and project should be [initiated](https://www.getlektor.com/docs/quickstart/).

## Theme-ing

**Step-1**: [Download](https://github.com/peeyushsrj/lektor-basic-theme/archive/master.zip) and extract it to `assets/static/` directory of your lektor project.

**Step-2**: Now add these information to `layout.html`.

e.g. Add below code between the `<head>` section of `layout.html` localted in `templates/` directory

```
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css'">
<link rel="stylesheet" href="{{ '/static/style.css'|url }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="{{ '/static/script.js'|url }}"></script>
```
